# nodebundle

![](https://img.shields.io/badge/written%20in-Javascript-blue)

Combine multiple js files with their dependencies into a single selfcontained script.

## Usage


```

Usage: nodebundle [options]

   -d {directory}       Add all files in a directory to bundle
   -e {file}            Execute file in bundle when bundle is executed
   -f {file}            Add a file to bundle
   -h,--help            Display this message
   -o {file}            Set output file. Use - for stdout
   -x {regexp}          Prune all added files matching regexp

```


## See Also

- Inspired by crcn/sardines: https://github.com/crcn/sardines
- Get the latest version from `npm`: https://www.npmjs.org/package/nodebundle


## Download

- [⬇️ nodebundle-1.0.0.tgz](dist-archive/nodebundle-1.0.0.tgz) *(2.43 KiB)*
